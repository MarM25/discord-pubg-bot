#FROM arm32v7/python:3.8-buster
FROM python:3.9.6-buster

# Workdir in image
WORKDIR /code

#Copy necessary files
COPY requirements.txt .

#Pip install
RUN pip install -r requirements.txt

#Copy src
COPY src/ .

#Run
CMD [ "python", "-u", "./discord_bot.py" ]