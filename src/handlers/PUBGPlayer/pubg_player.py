import json
import os
import requests
import time

from ..Database.db_handler import DB_Handler


class PubgPlayer:

    def __init__(self, discord_id, pubg_id, sum_assists, sum_boosts, sum_dbnos, sum_dailyKills, sum_dailyWins, sum_damageDealt,
                 sum_headshotKills, sum_heals, sum_kills, max_longestKill, max_longestTimeSurvived, sum_losses,
                 max_maxKillStreaks, max_mostSurvivalTime, sum_rankPoints, sum_revives, sum_rideDistance, sum_roadKills,
                 max_roundMostKills, sum_roundsPlayed, sum_suicides, sum_swimDistance, sum_teamKills, sum_timeSurvived,
                 sum_top10s, sum_vehicleDestroys, sum_walkDistance, sum_weaponsAcquired, sum_weeklyKills, sum_weeklyWins,
                 sum_winPoints, sum_wins, mode, season):
        self.discord_id =discord_id
        self.pubg_id = pubg_id
        self.sum_assists = sum_assists
        self.sum_boosts =sum_boosts
        self.sum_dnos = sum_dbnos
        self.sum_dailyKills = sum_dailyKills
        self.sum_dailyWins = sum_dailyWins
        self.sum_damageDealt = sum_damageDealt
        self.sum_headshotKills = sum_headshotKills
        self.sum_heals = sum_heals
        self.sum_kills = sum_kills
        self.max_longestKill = max_longestKill
        self.max_longestTimeSurvived = max_longestTimeSurvived
        self.sum_losses = sum_losses
        self.max_maxKillStreaks = max_maxKillStreaks
        self.max_mostSurvivalTime = max_mostSurvivalTime
        self.sum_rankPoints = sum_rankPoints
        self.sum_revives = sum_revives
        self.sum_rideDistance = sum_rideDistance
        self.sum_roadKills = sum_roadKills
        self.max_roundMostKills = max_roundMostKills
        self.sum_roundsPlayed = sum_roundsPlayed
        self.sum_suicides = sum_suicides
        self.sum_swimDistance = sum_swimDistance
        self.sum_teamKills = sum_teamKills
        self.sum_timeSurvived = sum_timeSurvived
        self.sum_top10s = sum_top10s
        self.sum_vehicleDestroys = sum_vehicleDestroys
        self.sum_walkDistance = sum_walkDistance
        self.sum_weaponsAcquired = sum_weaponsAcquired
        self.sum_weeklyKills = sum_weeklyKills
        self.sum_weeklyWins = sum_weeklyWins
        self.sum_winPoints = sum_winPoints
        self.sum_wins = sum_wins
        self.season = season
        self.mode = mode

    @staticmethod
    def register(author_id, pubg_name):
        if DB_Handler.query(f'SELECT discord_id FROM pubg_season_stats WHERE discord_id = {author_id}'):
            return None
        print()
        pubg_id = PubgPlayer.get_pubg_id_by_name(pubg_name)
        PubgPlayer.get_real_lifetime_stats(author_id, pubg_id)
        return 1

    @staticmethod
    def get_real_lifetime_stats(author_id, pubg_id):
        season = ''
        last_saved_season = DB_Handler.query('SELECT value FROM pubg_global WHERE data_key="season"')[0][0]
        i = 1
        while season != last_saved_season:
            season = f'division.bro.official.pc-2018-{i:02d}'
            season_player_data = json.loads(PubgPlayer.get_data(pubg_id, season).text)
            PubgPlayer.insert_season_answer(author_id, pubg_id, season, season_player_data)
            if season == last_saved_season:
                continue
            i += 1
            time.sleep(6)

    @staticmethod
    def get_data(pubg_id, season):
        req = f"https://api.pubg.com/shards/steam/players/{pubg_id}/seasons/{season}?filter[gamepad]=false"
        data = requests.get(req, headers={"Authorization": f'Bearer {os.getenv("PUBG_TOKEN")}',"accept": "application/vnd.api+json"})
        try:
            checkdata = json.loads(data.text)
        except:
            print(data)
            return
        if 'errors' in checkdata:
            raise ValueError('Username nicht gefunden')
        return data

    @staticmethod
    def get_pubg_id_by_name(username):
        header = {"Authorization": f'Bearer {os.getenv("PUBG_TOKEN")}', "accept": "application/vnd.api+json"}
        req = f'https://api.pubg.com/shards/steam/players?filter[playerNames]={username}'
        r = requests.get(req, headers=header)
        if r.status_code != 200:
            raise ValueError(f'HTTP-Code: {r.status_code}')
        player = json.loads(r.text)
        time.sleep(6)
        return player['data'][0]['id']

    @staticmethod
    def update_player_stats(author_id):
        pubg_id_query = f'SELECT pubg_id FROM pubg_season_stats WHERE discord_id = {author_id} GROUP BY pubg_id'
        pubg_id = DB_Handler.query(pubg_id_query)[0][0]
        season = DB_Handler.query(f'SELECT value FROM pubg_global WHERE data_key="season"')[0][0]
        season_player_data = json.loads(PubgPlayer.get_data(pubg_id, season).text)
        PubgPlayer.update_season_answer(author_id, season, season_player_data)

    @staticmethod
    def update_season_answer(author_id, season, response):
        for key, v in response['data']['attributes']['gameModeStats'].items():
            all_vals = []
            for item_key, item_value in v.items():
                all_vals.append([item_key, item_value if item_value else "null"])
            where_clause = f'discord_id={author_id} AND mode="{key}" AND season="{season}"'
            DB_Handler.update_wrappend('pubg_season_stats', where_clause, all_vals)

    @staticmethod
    def insert_season_answer(author_id, pubg_id, season, response):
        for key, v in response['data']['attributes']['gameModeStats'].items():
            all_keys = ["pubg_id", "discord_id", "season", "mode"]
            all_vals = [pubg_id, author_id, season, key]
            for item_key, item_value in v.items():
                all_keys.append(item_key)
                all_vals.append(item_value if item_value else None)
            DB_Handler.insert_wrappend('pubg_season_stats', all_keys, all_vals)

    @staticmethod
    def get_PubgPlayer(discord_id, mode=None, season=None):
        query = f"SELECT pubg_id, SUM(assists), SUM(boosts), SUM(dBNOs), SUM(dailyKills), SUM(dailyWins), SUM(damageDealt), SUM(headshotKills), SUM(heals), SUM(kills), MAX(longestKill), MAX(longestTimeSurvived), SUM(losses), MAX(maxKillStreaks), MAX(mostSurvivalTime), SUM(rankPoints), SUM(revives), SUM(rideDistance), SUM(roadKills), MAX(roundMostKills), SUM(roundsPlayed), SUM(suicides), SUM(swimDistance), SUM(teamKills), SUM(timeSurvived), SUM(top10s), SUM(vehicleDestroys), SUM(walkDistance), SUM(weaponsAcquired), SUM(weeklyKills), SUM(weeklyWins), SUM(winPoints), SUM(wins) FROM pubg_season_stats WHERE discord_id = \'{discord_id}\'"
        if mode != '' and mode is not None:
            query += f' and mode = \'{mode}\''
        if season is not None:
            query += f' and season = \'{season}\''
        query += ' GROUP BY pubg_id'
        try:
            ds = DB_Handler.query(query)[0]
            if not any(ds) or len(ds) < 32:
                return None
            ds = [0 if x is None else x for x in ds]
            return PubgPlayer(discord_id, ds[0], ds[1], ds[2], ds[3], ds[4], ds[5], ds[6], ds[7], ds[8], ds[9],
                              ds[10], ds[11], ds[12], ds[13], ds[14], ds[15], ds[16], ds[17], ds[18], ds[19], ds[20],
                              ds[21], ds[22], ds[23], ds[24], ds[25], ds[26], ds[27], ds[28], ds[29], ds[30], ds[31],
                              ds[32], mode, season)
        except Exception as e:
            print(str(e))
            return None

    @staticmethod
    def get_kd(kills, matches):
        if kills == 0 and matches == 0:
            return 0
        try:
            return round(kills / matches, 2)
        except ZeroDivisionError:
            return 0

    def format_discord_tag(self) -> str:
        return f'<@{self.discord_id}>'

    # This is a to_str, sadly it must be so cruel
    def playerstats(self):
        myname = self.format_discord_tag()
        mode = f' im Modus {self.mode}' if self.mode is not None else 'solo'

        k1, k2, k3, k4, k5, k6, k7, k8, k9, k10 = 'Kategorie', 'Matches', 'Wins', 'Kills', 'KD', 'AV Dmg', 'Headshots', 'Longest Distance Kills (m)',  'Most Kills in one Match', 'Teamkills'
        fill = ' '
        align = '<'
        width_k = 26
        width = 7

        out_str = [
            f'Daten von {myname} {mode}',
            ':\n' if self.season is None else f' aus Season {self.season}:\n\n',
            f'`\n'
            f'| {k1:{fill}{align}{width_k}}| {"":{fill}{align}{width}}|\n',
            f'| {k2:{fill}{align}{width_k}}| {self.sum_roundsPlayed:{fill}{align}{width}}|\n',
            f'| {k3:{fill}{align}{width_k}}| {self.sum_wins:{fill}{align}{width}}|\n',
            f'| {k4:{fill}{align}{width_k}}| {self.sum_kills:{fill}{align}{width}}|\n',
            f'| {k5:{fill}{align}{width_k}}| {PubgPlayer.get_kd(self.sum_kills, self.sum_roundsPlayed):{fill}{align}{width}}|\n',
            f'| {k6:{fill}{align}{width_k}}| {PubgPlayer.get_kd(self.sum_damageDealt, self.sum_roundsPlayed):{fill}{align}{width}}|\n',
            f'| {k7:{fill}{align}{width_k}}| {self.sum_headshotKills:{fill}{align}{width}}|\n',
            f'| {k8:{fill}{align}{width_k}}| {round(self.max_longestKill, 2):{fill}{align}{width}}|\n',
            f'| {k9:{fill}{align}{width_k}}| {self.max_roundMostKills:{fill}{align}{width}}|\n',
            f'| {k10:{fill}{align}{width_k}}| {self.sum_teamKills:{fill}{align}{width}}|\n`',
        ]
        return ''.join(out_str)

    # This is a to_str, sadly it must be so cruel
    def compare(self, other) -> str:
        myname = self.format_discord_tag()
        othername = other.format_discord_tag()
        mode = f' im Modus {self.mode}' if self.mode is not None else ''

        k1, k2, k3, k4, k5, k6, k7, k8, k9, k10 = 'Kategorie', 'Matches', 'Wins', 'Kills', 'KD', 'AV Dmg', 'Headshots', 'Longest Distance Kills (m)',  'Most Kills in one Match', 'Teamkills'

        fill = ' '
        align = '<'
        width_k = 26
        width = 7
        out_str = [
            f'Hier der Vergleich zwischen {myname} (links) and {othername} (rechts){mode}',
            ':\n' if self.season is None else f' aus Season {self.season}:\n\n',
            f'`\n'
            f'| {k1:{fill}{align}{width_k}}| {"":{fill}{align}{width}}| {"":{fill}{align}{width}}|\n',
            f'| {k2:{fill}{align}{width_k}}| {self.sum_roundsPlayed:{fill}{align}{width}}| {other.sum_roundsPlayed:{fill}{align}{width}}|\n',
            f'| {k3:{fill}{align}{width_k}}| {self.sum_wins:{fill}{align}{width}}| {other.sum_wins:{fill}{align}{width}}|\n',
            f'| {k4:{fill}{align}{width_k}}| {self.sum_kills:{fill}{align}{width}}| {other.sum_kills:{fill}{align}{width}}|\n',
            f'| {k5:{fill}{align}{width_k}}| {PubgPlayer.get_kd(self.sum_kills, self.sum_roundsPlayed):{fill}{align}{width}}| {PubgPlayer.get_kd(other.sum_kills, other.sum_roundsPlayed):{fill}{align}{width}}|\n',
            f'| {k6:{fill}{align}{width_k}}| {PubgPlayer.get_kd(self.sum_damageDealt, self.sum_roundsPlayed):{fill}{align}{width}}| {PubgPlayer.get_kd(other.sum_damageDealt, other.sum_roundsPlayed):{fill}{align}{width}}|\n',
            f'| {k7:{fill}{align}{width_k}}| {self.sum_headshotKills:{fill}{align}{width}}| {other.sum_headshotKills:{fill}{align}{width}}|\n',
            f'| {k8:{fill}{align}{width_k}}| {round(self.max_longestKill, 2):{fill}{align}{width}}| {round(other.max_longestKill, 2):{fill}{align}{width}}|\n',
            f'| {k9:{fill}{align}{width_k}}| {self.max_roundMostKills:{fill}{align}{width}}| {other.max_roundMostKills:{fill}{align}{width}}|\n',
            f'| {k10:{fill}{align}{width_k}}| {self.sum_teamKills:{fill}{align}{width}}| {other.sum_teamKills:{fill}{align}{width}}|\n`',
        ]
        return ''.join(out_str)
