import os
import mysql.connector
from dotenv import load_dotenv

from .DatabaseError import DatabaseError

load_dotenv()


class DB_Handler:

    @staticmethod
    def query(query, values=()):
        try:
            mydb = mysql.connector.connect(
                host=os.getenv('HOST'),
                user=os.getenv('MYSQL_USER'),
                password=os.getenv('MYSQL_PASSWORD'),
                database=os.getenv('MYSQL_DATABASE')
            )
            cur = mydb.cursor()
            cur.execute(query, values)
            return cur.fetchall()
        except Exception as e:
            raise DatabaseError(f'Cannot get Info from DB: {e}')

    @staticmethod
    def insert(query, values=()):
        try:
            mydb = mysql.connector.connect(
                host=os.getenv('HOST'),
                user=os.getenv('MYSQL_USER'),
                password=os.getenv('MYSQL_PASSWORD'),
                database=os.getenv('MYSQL_DATABASE')
            )
            cur = mydb.cursor()
            cur.execute(query, values)
            mydb.commit()
        except Exception as e:
            raise DatabaseError(f'Cannot save into DB: {e}')

    @staticmethod
    def insert_wrappend(table, rows_tuple: (), values_tuple: ()):
        if len(rows_tuple) != len(values_tuple):
            raise DatabaseError("The length of new data is not matching to the length of necessary data!")
        query = f'INSERT INTO {table} ({",".join(rows_tuple)}) VALUES ({("%s, " * len(rows_tuple))[:-2]})'
        try:
            mydb = mysql.connector.connect(
                host=os.getenv('HOST'),
                user=os.getenv('MYSQL_USER'),
                password=os.getenv('MYSQL_PASSWORD'),
                database=os.getenv('MYSQL_DATABASE')
            )
            cur = mydb.cursor()
            cur.execute(query, values_tuple)
            mydb.commit()
        except Exception as e:
            raise DatabaseError(f'Cannot save into DB: {e}\b{query}')


    @staticmethod
    def update_wrappend(table, where_clause, key_values: []):
        formatted_vals = [f'{n[0]}={n[1]}' for n in key_values]
        query = f'UPDATE {table} SET {",".join(formatted_vals)} WHERE {where_clause}'
        try:
            mydb = mysql.connector.connect(
                host=os.getenv('HOST'),
                user=os.getenv('MYSQL_USER'),
                password=os.getenv('MYSQL_PASSWORD'),
                database=os.getenv('MYSQL_DATABASE')
            )
            cur = mydb.cursor()
            cur.execute(query)
            mydb.commit()
        except Exception as e:
            raise DatabaseError(f'Cannot save into DB: {e}\b{query}')
