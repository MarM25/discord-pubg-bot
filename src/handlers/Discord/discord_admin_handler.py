import json

from handlers.Discord.discord_helpers import strip_id
from server_handler import ServerHandler


class AdminHandler:

    @staticmethod
    async def admin_handler(ctx, arg1=None, arg2=None):
        settings = list(ServerHandler.get_settings_by_guild_id(ctx.guild.id))
        settings[1] = json.loads(settings[1])
        if ctx.author.id not in settings[1]['controller']:
            await ctx.send('Du hast nicht die Rechte, dies zu tun.')
            return
        if arg2 is None:
            await ctx.send(f'Geben sie einen Nutzer mit @ an. Bsp.: !controller -add @MarM')
        if arg1 is None:
            await ctx.send(f'Geben sie einen Befehl mit Nutzer an. Bsp.: !controller -add @MarM')
        uid = strip_id(arg2)
        if arg1 == '-add':
            user_in = list(filter(lambda x: x == uid, settings[1]['controller']))
            if user_in is not None:
                await ctx.send(f'Der Nutzer ist schon berechtigt den Bot zu verwalten.')
                return
            settings[1]['controller'].append(uid)
            await ctx.send(f'Nutzer<@{uid}> hinzugefügt.')
        if arg1 == '-remove':
            if uid == ctx.guild.owner_id:
                await ctx.send(f'Der Server Owner kann nicht entfernt werden.')
                return
            user_in = list(filter(lambda x: x == uid, settings[1]['controller']))
            if not user_in:
                await ctx.send(f'Der Nutzer hatte schon keine Rechte, den Bot zu verwalten.')
                return
            settings[1]['controller'].remove(uid)
            await ctx.send(f'Nutzer<@{uid}> gelöscht.')

        settings[1] = json.dumps(settings[1])
        ServerHandler.update_server_settings(settings)