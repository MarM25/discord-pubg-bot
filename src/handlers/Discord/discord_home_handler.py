import json
import pytz
import requests
from discord.utils import get

from datetime import datetime as dt


class HomeHandler:

    @staticmethod
    async def check_home_is_online(bot):
        x = requests.get("http://51.124.202.14:5000/home")
        last_call_json = json.loads(x.content)
        last_call_dt = dt.strptime(last_call_json['last_call'], "%d.%m.%Y, %H:%M:%S")
        tz = pytz.timezone('Europe/Berlin')
        now = dt.now(tz)
        unixnow = now.timestamp()
        unixlast = last_call_dt.timestamp()
        last_call_td = unixnow - unixlast
        user = get(bot.get_all_members(), id=257102346784473088)
        if last_call_td > 900:
            await user.send(f'Raspberry PI hat sich länger nicht gemeldet. Checke Strom oder WLAN.\nDebug-Information'
                            f':\nNow: {now}\nLast Call: {last_call_dt}\nTD:{last_call_td} sec')
        print(f'{now}: {last_call_td}')
