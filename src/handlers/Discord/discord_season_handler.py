import json
import requests
import time
import os
from handlers.Database.db_handler import DB_Handler
from handlers.PUBGPlayer.pubg_player import PubgPlayer


class SeasonHandler:

    @staticmethod
    async def season_handler(bot):
        curr_season = DB_Handler.query('SELECT value FROM pubg_global WHERE data_key="season"')[0][0]
        req = f'https://api.pubg.com/shards/steam/seasons'
        data = requests.get(req, headers={"Authorization": f'Bearer {os.getenv("PUBG_TOKEN")}',
                                          "accept": "application/vnd.api+json"})
        checkdata = json.loads(data.text)
        if 'errors' in checkdata:
            raise ValueError('Seasons nicht gefunden')
        server_seasons = list(filter(lambda x: 'console' not in x['id'], checkdata['data']))
        server_season = list(filter(lambda x: x['attributes']['isCurrentSeason'], server_seasons))[0]
        if curr_season != server_season['id']:
            ids = DB_Handler.query("SELECT discord_id FROM pubg_season_stats GROUP BY discord_id")[0]
            # region
            # Update all old seasons
            for id in ids:
                PubgPlayer.update_player_stats(id)
                time.sleep(6)
            # endregion
            DB_Handler.insert("UPDATE pubg_global SET value=%s WHERE data_key=%s", (server_season['id'], 'season'))
            # region
            # Update all news seasons
            for id in ids:
                PubgPlayer.update_player_stats(id)
                time.sleep(6)
            # endregion
            server = (DB_Handler.query('SELECT out_channel, twitter_last FROM server WHERE ID =  571648354577285120'))[
                0]
            channel = bot.get_channel(server[0])
            await channel.send(f'Liebe User, eine neue Season hat begonnen!\n'
                               f'Eure !pubgdata Stats sind deswegen nun genullt. '
                               f'Ihr könnt eure Gesamtstats aber immer noch mit der *-o* und *-a* Option anzeigen lassen '
                               f'(siehe Pin)!\n\n'
                               f'Viel Erfolg, euer MarMBot.')