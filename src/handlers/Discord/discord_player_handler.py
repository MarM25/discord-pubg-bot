from datetime import datetime as dt
from discord.utils import find


from handlers.Database.db_handler import DB_Handler
from handlers.Discord.discord_helpers import strip_id
from handlers.PUBGPlayer.pubg_player import PubgPlayer
from PUBGError import PUBGError


class PlayerHandler:

    @staticmethod
    async def handle_playerstats(ctx, arg1, arg2=None, arg3=None):
        args = [arg1, arg2.lower() if arg2 is not None else '', arg3.lower() if arg3 is not None else '']
        season = DB_Handler.query(f'SELECT value FROM pubg_global WHERE data_key="season"')[0][0]
        if '-a' in args or '--all' in args:
            season = None

        mode = 'solo'
        player_id = None
        if '-o' in args or '--overall' in args:
            mode = None

        for i in range(0, 3):
            if 'solo' == args[i] or 'duo' == args[i] or 'squad' == args[i] or 'solo-fpp' == args[i] or 'duo-fpp' == args[i] or 'squad-fpp' == args[i]:
                mode = args[i]
            try:
                if not player_id:
                    player_id = strip_id(args[i])
            except:
                continue
        if not player_id:
            await ctx.send('Konnte Nutzer-ID nicht erkennen.')
            return

        player = PubgPlayer.get_PubgPlayer(player_id, mode, season)

        if not player:
            await ctx.send('Konnte Nutzer nicht finden. Bitte den Nutzer sich vorher an mir zu registrieren!')
            return

        PubgPlayer.update_player_stats(player_id)
        stats = player.playerstats()
        if mode is None:
            mode = 'Overall'
        if season is None:
            season = 'Lifetime'
        else:
            season = f'Season {season[-2:]}'
        await ctx.send(f'Die {season} {mode}-Stats von <@{player_id}>:\n{stats}')

    @staticmethod
    async def handle_pubgdata(ctx, arg1=None, arg2=None):
        args = [arg1.lower() if arg1 is not None else '', arg2.lower() if arg2 is not None else '']
        PubgPlayer.update_player_stats(ctx.author.id)

        season = DB_Handler.query(f'SELECT value FROM pubg_global WHERE data_key="season"')[0][0]
        if '-a' in args or '--all' in args:
            season = None

        mode = 'solo'
        if '-o' in args or '--overall' in args:
            mode = None
        for i in range(0, 2):
            if 'solo' == args[i] or 'duo' == args[i] or 'squad' == args[i] or 'solo-fpp' == args[i] or 'duo-fpp' == \
                    args[i] or 'squad-fpp' == args[i]:
                mode = args[i]

        player = PubgPlayer.get_PubgPlayer(ctx.author.id, mode, season)
        stats = player.playerstats()
        if mode is None:
            mode = 'Overall'
        if season is None:
            season = 'Lifetime'
        else:
            season = f'Season {season[-2:]}'
        await ctx.send(f'Die {season} {mode}-Stats von <@{ctx.author.id}>:\n{stats}')

    @staticmethod
    async def handle_compare(ctx, arg1, arg2=None, arg3=None):
        # TODO: Add Season and Mode Selection, must be parsed from args and used in fabricator
        try:
            p1 = PubgPlayer.get_PubgPlayer(ctx.author.id)
            p2 = PubgPlayer.get_PubgPlayer(strip_id(arg1))
        except Exception as e:
            await ctx.send(f'{str(e)}')
            return

        if p1 is None or p2 is None:
            await ctx.send('Konnte Spieler zum Vergleich nicht ermitteln.')
            return
        await ctx.send(p1.compare(p2))

    @staticmethod
    async def handle_dinners(ctx, arg1=None):
        start = dt.now().timestamp()
        args = [arg1.lower() if arg1 is not None else '']

        mode = 'solo'
        if '-o' in args or '--overall' in args:
            mode = None
        for i in range(0, 1):
            if 'solo' == args[i] or 'duo' == args[i] or 'squad' == args[i] or 'solo-fpp' == args[i] or 'duo-fpp' == \
                    args[i] or 'squad-fpp' == args[i]:
                mode = args[i]
        where_clause = f'WHERE mode = "{mode}"' if mode else ''
        query = f'SELECT discord_id, SUM(wins) AS SUM_WINS FROM pubg_season_stats {where_clause} GROUP BY discord_id ORDER BY SUM_WINS DESC'
        res = DB_Handler.query(query)

        max_print_once = 25  # Regarding the fact that max 2k signs shall send via api: Senc this amount with one post
        counter = 0
        res_dataset = []
        await ctx.send(f'Gesamte Server {mode} Dinner Stats:\n')
        for dataset in res:
            user = find(lambda m: m.id == dataset[0], ctx.guild.members)
            if user is None:
                continue
            res_dataset.append(dataset)
            counter += 1
            if counter == max_print_once:
                await ctx.send(''.join([f'<@{n[0]}>: {int(n[1]) if n[1] else 0} Chicken Dinner\n' for n in res_dataset]))
                counter = 0
                res_dataset = []

        await ctx.send(''.join([f'<@{n[0]}>: {int(n[1]) if n[1] else 0} Chicken Dinner\n' for n in res_dataset]))
        await ctx.send('\n\nDu vermisst dich in dieser Liste? Registriere dich am MarMBot! (Siehe Pin)')
        end = dt.now().timestamp()
        print(end - start)
        return

    @staticmethod
    async def get_list_tk(ctx):
        query = 'SELECT discord_id, SUM(roundsPlayed) as srp, SUM(teamKills) as stk ' \
                'FROM pubg_season_stats GROUP BY discord_id ORDER BY stk DESC;'
        res = DB_Handler.query(query)

        max_print_once = 25  # Regarding the fact that max 2k signs shall send via api: Senc this amount with one post
        counter = 0
        res_dataset = []
        await ctx.send(f'Gesamte Server Teamkill Stats:\n')
        for dataset in res:
            user = find(lambda m: m.id == dataset[0], ctx.guild.members)
            if user is None or dataset[2] is None:
                continue
            res_dataset.append(dataset)
            counter += 1
            if counter == max_print_once:
                await ctx.send(''.join([f'<@{n[0]}>: {n[2] if n[2] else 0} Teamkills ({round(n[1] / n[2], 2) if n[1] and n[2] else 0} Matches pro Teamkill)\n' for n in res_dataset]))
                counter = 0
                res_dataset = []
        await ctx.send(''.join([f'<@{n[0]}>: {n[2] if n[2] else 0} Teamkills ({round(n[1] / n[2], 2) if n[1] and n[2] else 0} Matches pro Teamkill)\n' for n in res_dataset]))
        await ctx.send('\n\nDu vermisst dich in dieser Liste? Registriere dich am MarMBot! (Siehe Pin)')


    @staticmethod
    async def get_longest_kill(ctx):
        query='SELECT discord_id, MAX(longestKill) as mlk FROM pubg_season_stats GROUP BY discord_id ORDER BY mlk DESC'
        res = DB_Handler.query(query)

        max_print_once = 25  # Regarding the fact that max 2k signs shall send via api: Senc this amount with one post
        counter = 0
        res_dataset = []
        await ctx.send(f'Server Längster kill Stats:\n')
        for dataset in res:
            user = find(lambda m: m.id == dataset[0], ctx.guild.members)
            if user is None:
                continue
            res_dataset.append(dataset)
            counter += 1
            if counter == max_print_once:
                await ctx.send(''.join([f'<@{n[0]}>: {round(n[1], 2)}m\n' for n in res_dataset]))
                counter = 0
                res_dataset = []
        await ctx.send(''.join([f'<@{n[0]}>: {round(n[1], 2)}m\n' for n in res_dataset]))
        await ctx.send('\n\nDu vermisst dich in dieser Liste? Registriere dich am MarMBot! (Siehe Pin)')

    @staticmethod
    async def get_suicides(ctx):
        query = 'SELECT discord_id, SUM(suicides) as ss FROM pubg_season_stats GROUP BY discord_id ORDER BY ss DESC;'
        res = DB_Handler.query(query)

        max_print_once = 25  # Regarding the fact that max 2k signs shall send via api: Senc this amount with one post
        counter = 0
        res_dataset = []
        await ctx.send(f'Server Suicide Stats:\n')
        for dataset in res:
            user = find(lambda m: m.id == dataset[0], ctx.guild.members)
            if user is None:
                continue
            res_dataset.append(dataset)
            counter += 1
            if counter == max_print_once:
                await ctx.send(''.join([f'<@{n[0]}>: {n[1] if n[1] else 0} suicides\n' for n in res_dataset]))
                counter = 0
                res_dataset = []
        await ctx.send(''.join([f'<@{n[0]}>: {n[1] if n[1] else 0} suicides\n' for n in res_dataset]))
        await ctx.send('\n\nDu vermisst dich in dieser Liste? Registriere dich am MarMBot! (Siehe Pin)')

    @staticmethod
    async def get_matches(ctx):
        query = 'SELECT discord_id, SUM(roundsPlayed) as ss FROM pubg_season_stats GROUP BY discord_id ORDER BY ss DESC;'
        res = DB_Handler.query(query)

        max_print_once = 25  # Regarding the fact that max 2k signs shall send via api: Senc this amount with one post
        counter = 0
        res_dataset = []
        await ctx.send(f'Server Match Stats:\n')
        for dataset in res:
            user = find(lambda m: m.id == dataset[0], ctx.guild.members)
            if user is None:
                continue
            res_dataset.append(dataset)
            counter += 1
            if counter == max_print_once:
                await ctx.send(''.join([f'<@{n[0]}>: {n[1] if n[1] else 0} Matches\n' for n in res_dataset]))
                counter = 0
                res_dataset = []
        await ctx.send(''.join([f'<@{n[0]}>: {n[1] if n[1] else 0} Matches\n' for n in res_dataset]))
        await ctx.send('\n\nDu vermisst dich in dieser Liste? Registriere dich am MarMBot! (Siehe Pin)')

    @staticmethod
    async def handle_register(ctx, arg1):
        await ctx.send('Ziehe Daten der PUBG-API, bitte warten...')
        try:
            retval = PubgPlayer.register(ctx.author.id, arg1)
        except PUBGError as p:
            await ctx.send(f'Die PUBG-API hat deine Accountdaten nicht ausliefern können. Meldung: {p}')
            return
        except ValueError as e:
            await ctx.send(f'{e}, Daten wurden nicht gezogen')
            return
        if not retval:
            await ctx.send('Du bist schon registriert.')
            return
        await ctx.send('Done')


