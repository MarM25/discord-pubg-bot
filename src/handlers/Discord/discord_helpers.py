def strip_id(dc_id) -> int:
    return int(dc_id[3:-1])


def get_pubg_id(activity):
    return activity.party['id'][0:-9]
