from dotenv import load_dotenv

load_dotenv()


class ArgsHandler:

    def __init__(self, raw_args):
        self.raw_args = raw_args
        self.mode = None
        self.season = None

    @staticmethod
    def handle_pubg_args(*args):
        args = [a.lower() for a in args]
        ah = ArgsHandler(raw_args=args)
        ah.get_mode()
        ah.get_season()
        return ah

    def __get_index(self, searched_option: str):
        try:
            return self.raw_args.index(searched_option)
        except ValueError:
            return None

    def __get_value_from_option(self, option_index: int):
        if option_index + 1 >= len(self.raw_args):
            return None
        return self.raw_args[option_index]

    def get_mode(self):
        if self.__get_index('-o') is not None or self.__get_index('--overall') is not None:
            self.mode = None

    def get_season(self):
        if self.__get_index('-a') is not None or self.__get_index('--all_seasons') is not None:
            self.season = None
