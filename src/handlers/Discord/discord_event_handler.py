import time
from datetime import datetime as dt
from discord.utils import get

from handlers.Database.db_handler import DB_Handler
from handlers.PUBGPlayer.pubg_player import PubgPlayer


class DiscordEventHandler:

    @staticmethod
    def handle_member_remove(bot, marm_id, member):
        user = get(bot.get_all_members(), id=marm_id)
        user.send(f'{member.display_name} hat den Server {member.guild.name} verlassen.')

    @staticmethod
    def handle_member_update(after, before):
        update = False
        if hasattr(after.activity, 'application_id') and after.activity.application_id == 530196305138417685:
            if update:
                print(after.display_name)
                pubg_id = (after.activity.party['id']).split('-')[0]
                count = DB_Handler.query(f'SELECT COUNT(*) FROM pubg_season_stats where discord_id = {after.id}')[0][0]
                last_saved_season = DB_Handler.query('SELECT value FROM pubg_global WHERE data_key="season"')[0][0]
                if count < int(last_saved_season[-2:]) * 6:
                    print('missing data, deleting')
                    DB_Handler.query(f'DELETE FROM pubg_season_stats where discord_id = {after.id}')
                if DB_Handler.query(f'SELECT discord_id FROM pubg_season_stats WHERE discord_id = {after.id}'):
                    print('Skipping')
                    return
                print('Storing')
                PubgPlayer.get_real_lifetime_stats(after.id, pubg_id)
                print('Stored')
                time.sleep(6)
                if (before.activity.details is not None and after.activity.details is not None) and (
                        before.activity.details != after.activity.details):
                    print(f'{dt.now().strftime("%d.%m.%Y %H:%M:%S")} - {after.display_name}: {after.activity.details}')
