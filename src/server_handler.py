import json, os, mysql.connector
from dotenv import load_dotenv

load_dotenv()

class ServerHandler:

    @staticmethod
    def join_server(guild_id, guild_owner_id):
        mydb = mysql.connector.connect(
            host=os.getenv('HOST'),
            user=os.getenv('MYSQL_USER'),
            password=os.getenv('MYSQL_PASSWORD'),
            database=os.getenv('DATABASE')
        )
        cur = mydb.cursor()
        val = (guild_id, json.dumps({'controller': [guild_owner_id]}), 0, 0)
        cur.execute("INSERT INTO server (id, controllers, command_channel, out_channel) VALUES (%s, %s, %s, %s)", val)
        mydb.commit()

    @staticmethod
    def get_settings_by_guild_id(guild_id: int):
        mydb = mysql.connector.connect(
            host=os.getenv('HOST'),
            user=os.getenv('MYSQL_USER'),
            password=os.getenv('MYSQL_PASSWORD'),
            database=os.getenv('DATABASE')
        )
        cur = mydb.cursor()
        cur.execute('SELECT * FROM server')
        rows = cur.fetchall()
        settings = list(filter(lambda x: x[0] == guild_id, rows))
        if settings is not None:
            return settings[0]

    @staticmethod
    def update_server_settings(values):
        id = values[0]
        values.pop(0)
        values.append(id)
        mydb = mysql.connector.connect(
            host=os.getenv('HOST'),
            user=os.getenv('MYSQL_USER'),
            password=os.getenv('MYSQL_PASSWORD'),
            database=os.getenv('DATABASE')
        )
        cur = mydb.cursor()
        query = 'UPDATE server set controllers = %s, command_channel = %s, out_channel = %s WHERE id = %s'
        cur.execute(query, tuple(values))
        mydb.commit()
