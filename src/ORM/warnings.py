from datetime import datetime as dt

from handlers.Database.db_handler import DB_Handler
from handlers.Discord.discord_helpers import strip_id


class warnings:

    def __init__(self, d_id, create_time, update_time, server_id, user_id, warner_id, reason, is_active):
        self.d_id = d_id
        self.create_time = create_time
        self.update_time = update_time
        self.server_id = server_id
        self.user_id = user_id
        self.warner_id = warner_id
        self.reason = reason
        self.is_active = is_active

    @staticmethod
    def create(ctx, arg1, arg2):
        now = dt.now()
        user_id = strip_id(arg1)
        if not isinstance(user_id, int):
            return None
        if arg2 is None or not isinstance(arg2, str):
            reason = ''
        else:
            reason = arg2
        return warnings(None, now, now, ctx.guild.id, user_id, ctx.author.id, reason, True)

    def store(self):
        try:
            DB_Handler.insert_wrappend('warnings',
            ('create_time', 'update_time', 'server_id', 'user_id', 'warner_id', 'reason', 'is_active'),
            (self.create_time.strftime('%Y-%m-%d %H:%M:%S'),
             self.update_time.strftime('%Y-%m-%d %H:%M:%S'),
             self.server_id, self.user_id, self.warner_id, self.reason, self.is_active)
            )
            return True
        except Exception as e:
            print(e)
        return False



