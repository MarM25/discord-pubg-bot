from datetime import datetime as dt
from dotenv import load_dotenv

from handlers.Database.db_handler import DB_Handler

load_dotenv()


class StatsHandler:

    @staticmethod
    def log_command(author, command):
        query = 'INSERT INTO stats (call_time, author, command) VALUES(%s, %s, %s)'
        DB_Handler.insert(query, (dt.now(), author, command))


    @staticmethod
    def get_general_statistics():
        query_calls = 'SELECT command, COUNT(command) FROM stats GROUP BY command ORDER BY COUNT(COMMAND) DESC'
        query_author_calls = 'SELECT author, COUNT(author) FROM stats GROUP BY author ORDER BY COUNT(author) DESC'
        x = DB_Handler.query(query_calls)
        y = DB_Handler.query(query_author_calls)
        return x, y

    @staticmethod
    def get_user_statistics(author_id):
        query_author_fav_calls = f'SELECT command, COUNT(command) FROM stats WHERE author = {author_id} GROUP BY command ORDER BY COUNT(command) DESC'
        return DB_Handler.query(query_author_fav_calls)
