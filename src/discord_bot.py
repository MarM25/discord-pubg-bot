import aiocron, discord, os
from datetime import datetime as dt
from discord.ext import commands
from dotenv import load_dotenv

from handlers.Discord.discord_event_handler import DiscordEventHandler
from handlers.Discord.discord_player_handler import PlayerHandler
from handlers.Discord.discord_home_handler import HomeHandler
from handlers.Discord.discord_admin_handler import AdminHandler
from handlers.Discord.discord_season_handler import SeasonHandler

from server_handler import ServerHandler

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
marm_id = 257102346784473088
bot = commands.Bot(command_prefix="!", case_insensitive=True, intents=(discord.Intents().all()))


@bot.command(name='controller', description="Befehl zur Verwaltung der User, die diesen Bot verändern dürfen")
async def adm_controller(ctx, arg1=None, arg2=None):
    await AdminHandler.admin_handler(ctx, arg1, arg2)


@bot.command(name='playerstats', description="Zeige die Server-Statistik eines Mitglieds an")
async def playerstats(ctx, arg1, arg2=None, arg3=None):
    await PlayerHandler.handle_playerstats(ctx, arg1, arg2, arg3)


@bot.command(name='pubgdata', description="Zeige die PUBG-Statistik von dir.")
async def pubgdata(ctx, arg1=None, arg2=None):
    await PlayerHandler.handle_pubgdata(ctx, arg1, arg2)


@bot.command(name='compare', description="Vergleiche deine PUBG-Statistik mit anderen Servermembern!")
async def compare(ctx, arg1, arg2=None, arg3=None):
    await PlayerHandler.handle_compare(ctx, arg1, arg2, arg3)


@bot.command(name='dinners', description="Dinner Stats from today")
async def dinners(ctx, arg1=None):
    await PlayerHandler.handle_dinners(ctx, arg1)


@bot.command(name='teamkills', description="Teamkills Stats")
async def teamkills(ctx):
    await PlayerHandler.get_list_tk(ctx)


@bot.command(name='longest', description="Longest kills Stats")
async def longestkills(ctx):
    await PlayerHandler.get_longest_kill(ctx)


@bot.command(name='suicides', description="Suicide Stats")
async def suicides(ctx):
    await PlayerHandler.get_suicides(ctx)


@bot.command(name='matches', description="Match Stats")
async def suicides(ctx):
    await PlayerHandler.get_matches(ctx)

@bot.command(name='register', description="Mappe deinen PUBG-Account")
async def registerme(ctx, arg1):
    await PlayerHandler.handle_register(ctx, arg1)


@bot.command(name='updateseason', description="Update die PUBG-Season")
async def updateseason(ctx):
    await SeasonHandler.season_handler(bot)


@aiocron.crontab('*/15 * * * *')
async def homecheck():
    await HomeHandler.check_home_is_online(bot)


@aiocron.crontab('0 3 * * *')
async def seasonjob():
    await SeasonHandler.season_handler(bot)


@bot.event
async def on_guild_join(guild):
    ServerHandler.join_server(guild.id, guild.owner_id)


@bot.event
async def on_member_remove(member):
    DiscordEventHandler.handle_member_remove(bot, marm_id, member)


@bot.event
async def on_member_update(before, after):
    DiscordEventHandler.handle_member_update(before, after)


@bot.event
async def on_voice_state_update(member, before, after):
    print(f'{dt.now().strftime("%d.%m.%Y %H:%M:%S")}: {member.display_name}\n{str(before)}\n{str(after)}\n\n')

bot.run(TOKEN)
