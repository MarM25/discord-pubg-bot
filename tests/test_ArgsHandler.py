import unittest
from unittest.mock import patch

from src.handlers.Discord.args_handler import ArgsHandler


class TestArgsHandler(unittest.TestCase):

    # patch('src.handlers.Database.db_handler.DB_Handler.query')
    def test_translate_args_o(self):
        mode = '-o'
        pubg_args = ArgsHandler.handle_pubg_args(mode)
        self.assertIsNone(pubg_args.mode)

    def test_translate_args_overall(self):
        mode = '--overall'
        pubg_args = ArgsHandler.handle_pubg_args(mode)
        self.assertIsNone(pubg_args.mode)

    def test_translate_args_a(self):
        mode = '-a'
        pubg_args = ArgsHandler.handle_pubg_args(mode)
        self.assertIsNone(pubg_args.mode)

    def test_translate_args_all_seasons(self):
        mode = '--all_seasons'
        pubg_args = ArgsHandler.handle_pubg_args(mode)
        self.assertIsNone(pubg_args.mode)