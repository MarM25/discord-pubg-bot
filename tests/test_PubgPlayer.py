import unittest
from unittest.mock import patch

from src.handlers.PUBGPlayer.pubg_player import PubgPlayer


class TestPubgPlayer(unittest.TestCase):

    @patch('src.handlers.Database.db_handler.DB_Handler.query')
    def test_get_pubg_id(self, mock_db_handler):
        mock_discord_id = 141949320369930240
        mock_pubg_id = 'account.c47a4fd64d7a4db7afab796f5f13139d'
        mock_db_handler.side_effect = [[(mock_pubg_id, )], [[mock_discord_id, mock_pubg_id, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]]]
        player = PubgPlayer.get_PubgPlayer(mock_discord_id)
        self.assertEqual(player.pubg_id, mock_pubg_id)
        self.assertEqual(player.discord_id, mock_discord_id)

    @patch('src.handlers.Database.db_handler.DB_Handler.query')
    def test_get_pubg_id_only_none(self, mock_db_handler):
        mock_discord_id = 141949320369930240
        mock_pubg_id = 'account.c47a4fd64d7a4db7afab796f5f13139d'
        mock_db_handler.side_effect = [[(mock_pubg_id, )], [[None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]]]
        player = PubgPlayer.get_PubgPlayer(mock_discord_id)
        self.assertIsNone(player)

    @patch('src.handlers.Database.db_handler.DB_Handler.query')
    def test_get_pubg_id_to_less(self, mock_db_handler):
        mock_discord_id = 141949320369930240
        mock_pubg_id = 'account.c47a4fd64d7a4db7afab796f5f13139d'
        mock_db_handler.side_effect = [[(mock_pubg_id, )], [[None]]]
        player = PubgPlayer.get_PubgPlayer(mock_discord_id)
        self.assertIsNone(player)

    @patch('src.handlers.Database.db_handler.DB_Handler.query')
    def test_get_pubg_id_not_found(self, mock_db_handler):
        mock_discord_id = 141949320369930240
        mock_db_handler.return_value = []
        player = PubgPlayer.get_PubgPlayer(mock_discord_id)
        self.assertIsNone(player)


    def test_create_table(self):
        myname = 'Marvin'
        othername = 'MaloMcFly'
        data = [
            ['Matches', 467, 231]
        ]
