# Discord Python Bot

This is a experimental software for a PUBG Discord bot, developed for the german Discord channel "Gaming Lounge".

If you want to contribute to this project, please contact me on this server.

The project is developed under Linux with python 3.8. Other python versions/operation systems are not supported from me, feel free if you want to try to develop on other systems.

## Prerequisites
* python3.8
* pip
* Discord token

## How to contribute
You will need the Discord bot token, which will provided by me. If you want to develop, plase contact me.

## How to develop
* Ensure you have installed the prerequisites.
* Run ```pip install -r requirements.txt```. This will install all the neccessary dependencies.

## Ho to Docker
### Docker Contribute

* Build: ```docker build -t pubg_discord .```
* Tag: ```docker tag pubg_discord marm25/pubg_discord```
* Push: ```docker push marm25/pubg_discord```

### Docker-Compose pull
* Delete old image: ```docker images``` and then ```docker rmi``` with the correct IMAGE ID.
* Build new: ```docker-compose rm -f bot && docker-compose build --pull && docker-compose up -d```


## Commands

* ```!flipcoin```: Flips a coin.
* ```!controller```: Edit the user permissions to use bot commands. In the following the options are given.
    *  ```-add```: Add a controller.
    *  ```-remove```: Remove a controller.
* ```!mystats```: Show your PUBG Stats 
* ```!playerstats```: Show others PUBG Stats 
* ```!dinnerstoday```: Show Server Dinners today
* ```!dinners```: Show Server Dinners
    * ```-days```: Period Stats.
